package com.bsa.bsagiphy.controller;

import com.bsa.bsagiphy.dto.DirDto;
import com.bsa.bsagiphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RequestMapping("/cache")
@RestController
public class CacheController {
    private CacheService cacheService;
    private Converter<Map<String, List<String>>, List<DirDto>> converter;

    public CacheController(@Autowired CacheService cacheService, Converter<Map<String, List<String>>, List<DirDto>> converter) {
        this.cacheService = cacheService;
        this.converter = converter;
    }

    @RequestMapping(method = GET)
    public ResponseEntity<List<DirDto>> getCacheFromDisk(@RequestParam(required = false) Map<String,String> query){

        String keyWord = query.get("query");
        Map<String, List<String>> cache = cacheService.getCache(keyWord);
        return new ResponseEntity<>(converter.convert(cache), HttpStatus.OK);

    }

    @PostMapping(value = "/generate")
    public ResponseEntity<DirDto> getGifFromGiphy(@RequestBody Map<String, String> body){
        Map<String, List<String>> query = cacheService.createGifAndReturnDir(body.get("query"));
        return new ResponseEntity<>(converter.convert(query).stream().findAny().get(), HttpStatus.CREATED);

    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = DELETE)
    public void cleanCache(){
        cacheService.cleanCache();
    }


}
