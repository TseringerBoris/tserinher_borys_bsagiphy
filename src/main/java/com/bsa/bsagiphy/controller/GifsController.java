package com.bsa.bsagiphy.controller;

import com.bsa.bsagiphy.service.CacheService;
import com.bsa.bsagiphy.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@RestController
@RequestMapping("/gifs")
public class GifsController {

    private CacheService cacheService;
    private UserService userService;

    public GifsController(CacheService cacheService, UserService userService) {
        this.cacheService = cacheService;
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<String>> getAllGifs() {
        List<String> gifs = Stream.of(userService.findAllGifs(), cacheService.findAllGifs())
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        return ResponseEntity.ok(gifs);

    }
}
