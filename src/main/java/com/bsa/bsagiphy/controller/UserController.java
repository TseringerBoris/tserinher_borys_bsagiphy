package com.bsa.bsagiphy.controller;

import com.bsa.bsagiphy.model.HistoryEntity;
import com.bsa.bsagiphy.service.CacheService;
import com.bsa.bsagiphy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.bsa.bsagiphy.dto.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private Converter<Map<String, List<String>>, List<DirDto>> converter;
    private Converter<List<HistoryEntity>, List<HistoryDto>> historyConverter;

    public UserController(@Autowired UserService userService, Converter<Map<String, List<String>>, List<DirDto>> converter, Converter<List<HistoryEntity>, List<HistoryDto>> historyConverter) {
        this.userService = userService;
        this.converter = converter;
        this.historyConverter = historyConverter;
    }

    @RequestMapping(value = "/{id}/all", method = RequestMethod.GET)
    public ResponseEntity<List<DirDto>> getAllGifsById(@PathVariable("id") String id) {

        Map<String, List<String>> allFiles = userService.getAllFilesBy(id);
        return new ResponseEntity<>(converter.convert(allFiles), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/history", method = RequestMethod.GET)
    public ResponseEntity<List<HistoryDto>> getUserHistoryById(@PathVariable("id") String userName) throws IOException {
        List<HistoryEntity> userHistory = userService.getUserHistory(userName);
        return new ResponseEntity<>(historyConverter.convert(userHistory), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}/history/clean", method = RequestMethod.DELETE)
    public void cleanUserHistoryById(@PathVariable("id") String userName) {
        userService.cleanUserHistory(userName);
    }

    @RequestMapping(value = "/{id}/search", method = RequestMethod.GET)
    public ResponseEntity<String> findGif(@PathVariable("id") String userName, @RequestParam Map<String, String> query) {
        String keyWord = query.get("query");
        boolean force = Boolean.parseBoolean(query.get("force"));
        String gif = userService.findGif(userName, keyWord, force);
        return new ResponseEntity<>(gif, HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/generate")
    public ResponseEntity<String> generateGif(@PathVariable("id") String userName, @RequestBody Map<String, Object> body) throws IOException {
        String keyWord = body.get("query").toString();
        Boolean force = (Boolean) body.get("force");
        String gif = userService.createGif(userName, keyWord, force);

        return new ResponseEntity<>(gif, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/reset", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void cleanUserCacheByKey(@PathVariable("id") String userName, @RequestParam Map<String, String> query) {
        userService.cleanUserCache(userName, query.get("query"));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}/clean", method = RequestMethod.DELETE)
    public void cleanUserData(@PathVariable("id") String userName) {
        userService.cleanUserCache(userName, null);
        userService.deleteUserData(userName);
    }


}
