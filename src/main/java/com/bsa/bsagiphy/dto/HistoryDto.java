package com.bsa.bsagiphy.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class HistoryDto {
    private String date;
    private String key;
    private String gif;

}
