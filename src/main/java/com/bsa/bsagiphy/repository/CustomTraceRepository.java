package com.bsa.bsagiphy.repository;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
public class CustomTraceRepository extends InMemoryHttpTraceRepository {

    @Override
    public void add(HttpTrace trace) {
        log.info(trace.getRequest());
        super.add(trace);
    }
}
