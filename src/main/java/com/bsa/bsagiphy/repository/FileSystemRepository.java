package com.bsa.bsagiphy.repository;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface FileSystemRepository {

    boolean save(String userName, String keyWord, File gif);

    boolean deleteByKeyWord(String userName, String keyWord);

    Map<String, List<String>> findAll() throws IOException;

    Map<String, List<String>> findAllByUserName(String userName);

    String find(String userName, String path);

}
