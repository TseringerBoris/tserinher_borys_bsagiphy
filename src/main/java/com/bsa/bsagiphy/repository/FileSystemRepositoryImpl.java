package com.bsa.bsagiphy.repository;

import com.bsa.bsagiphy.exception.NotFoundException;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class FileSystemRepositoryImpl implements FileSystemRepository {
    private static final String USERS_DIR_PATH = "./src/main/java/com/bsa/bsagiphy/bsa_giphy/users";

    public static String getUsersDirPath() {
        return USERS_DIR_PATH;
    }

    @Override
    public boolean save(String userName, String keyWord, File gif) {
        try {
            FileUtils.copyFileToDirectory(gif, new File(USERS_DIR_PATH + "/" + userName + "/" + keyWord));
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteByKeyWord(String userName, String keyWord) {
        try {
            if (StringUtils.isEmpty(keyWord)) {
                FileUtils.cleanDirectory(getChildDir(userName));
            } else {
                FileUtils.cleanDirectory(getChildDir(userName + "/" + keyWord));
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public Map<String, List<String>> findAll(){

        return listFiles(Path.of(USERS_DIR_PATH));
    }

    @Override
    public Map<String, List<String>> findAllByUserName(String userName) {
        return listFiles(Path.of(USERS_DIR_PATH + "/" + userName));
    }

    @Override
    public String find(String userName, String keyWord) {
        File childDir = getChildDir(userName + "/" + keyWord);
        return FileUtils.listFiles(childDir, null, true).stream()
                /*.filter(file -> keyWord.equals(file.getName()))*/
                .findAny()
                .orElseThrow(NotFoundException::new)
                .getAbsolutePath();
//        return Objects.isNull(result) ? null : result.getAbsolutePath();
    }

    private File getChildDir(String keyWord) {
        return new File(USERS_DIR_PATH + "/" + keyWord);
    }

    @SneakyThrows
    private Map<String, List<String>> listFiles(final Path baseDir) {
        final Map<String, List<String>> map = new HashMap<>();
        try (final DirectoryStream<Path> stream = Files.newDirectoryStream(baseDir)) {
            for (final Path subdir : stream)
                populateMap(map, subdir);
        }

        return map;
    }

    @SneakyThrows
    private void populateMap(final Map<String, List<String>> map, final Path subdir) {
        final String dirname = subdir.getFileName().toString();
        if (Files.isDirectory(subdir)) {
            map.put(dirname, new ArrayList<>());
            try (final DirectoryStream<Path> stream = Files.newDirectoryStream(subdir)) {
                for (final Path entry : stream) {
                    if (Files.isDirectory(entry)) {
                        populateMap(map, entry);
                    }
                    if (!Files.isDirectory(entry)) {
                        map.get(dirname).add(entry.getFileName().toString());
                    }
                }
            }
        }
    }
}
