package com.bsa.bsagiphy.repository;

import com.bsa.bsagiphy.exception.NotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.*;

@Repository
public class CacheInMemoryRepositoryImpl implements CacheInMemoryRepository {
    private Map<String, Map<String, List<String>>> cache = new HashMap<>();

    @Override
    public boolean cleanCacheByKeyWord(String userName, String keyWord) {
        if (!cache.containsKey(userName)) {
            return false;
        }
        if (StringUtils.isEmpty(keyWord)) {
            deleteCache(userName);
        }
        Optional.ofNullable(cache.get(userName).get(keyWord))
                .ifPresent(List::clear);
        return true;
    }

    @Override
    public boolean deleteCache(String userName) {
        if (cache.containsKey(userName)) {
            cache.get(userName).entrySet().clear();
            return true;
        }
        return false;
    }

    @Override
    public void save(String userName, String keyWord, File file) {
        addUser(userName);
        Map<String, List<String>> userDir = cache.get(userName);
        if (!userDir.containsKey(keyWord)) {
            userDir.put(keyWord, new ArrayList<>());
        }
        List<String> strings = userDir.get(keyWord);
        if(!strings.contains(file.getAbsolutePath())){
            strings.add(file.getAbsolutePath());
        }
    }

    @Override
    public String find(String userName, String keyWord) {

        return Optional.ofNullable(cache.get(userName))
                .map(stringListMap -> stringListMap.get(keyWord))
                .stream()
                .flatMap(Collection::stream)
                .findAny()
                .orElseThrow(NotFoundException::new);
    }

    public String addUser(String userName) {
        if (!cache.containsKey(userName)) {
            cache.put(userName, new HashMap<>());
        }
        return userName;
    }

}
