package com.bsa.bsagiphy.repository;

import com.bsa.bsagiphy.model.HistoryEntity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface HistoryRepository {

    boolean save(String userName, HistoryEntity history) throws IOException;

    boolean delete(String userName);

    List<HistoryEntity> findByUser(String user) throws IOException;
}
