package com.bsa.bsagiphy.repository;

import com.bsa.bsagiphy.dto.HistoryDto;
import com.bsa.bsagiphy.model.HistoryEntity;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class HistoryRepositoryImpl implements HistoryRepository {
    private static final String USERS_DIR_PATH = "./src/main/java/com/bsa/bsagiphy/bsa_giphy/users";

    @Override
    public boolean save(String userName, HistoryEntity history) throws IOException {
        FileWriter writer = new FileWriter(getChildDir(userName) + "/history.csv", true);
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(','));
        csvPrinter.printRecord(history.getDate(), history.getKeyWord(), history.getPathToGif());
        csvPrinter.flush();
        return true;
    }

    @Override
    public boolean delete(String userName) {
        try {
            Files.delete(Path.of(getChildDir(userName) + "/history.csv"));
            Files.createFile(Path.of(getChildDir(userName) + "/history.csv"));
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    @Override
    public List<HistoryEntity> findByUser(String user) throws IOException {
        String pathTOFile = getChildDir(user) + "/history.csv";
        Reader in = new FileReader(pathTOFile);
        Iterable<CSVRecord> history = CSVFormat.DEFAULT
                .withDelimiter(',')
                .parse(in);
        List<HistoryEntity> histories = new ArrayList<>();
        for (CSVRecord record : history) {
            histories.add(new HistoryEntity(LocalDate.parse(record.get(0)), record.get(1), record.get(2)));
        }
        return histories;
    }

    private String getChildDir(String userName) {
        return USERS_DIR_PATH + "/" + userName;
    }

}
