package com.bsa.bsagiphy.repository;

import com.bsa.bsagiphy.exception.NotFoundException;
import kong.unirest.Unirest;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import static com.bsa.bsagiphy.constant.Constants.FILE_NOT_FOUND;

@Repository
public class CacheRepositoryImpl implements CacheRepository {
    private static final String CACHE_DIR_PATH = "./src/main/java/com/bsa/bsagiphy/bsa_giphy/cache";
    private static final File CACHE_DIR = new File(CACHE_DIR_PATH);


    public static String getCacheDirPath() {
        return CACHE_DIR_PATH;
    }

    /*@Override
    public List<String> find(String keyWord) {
        if(StringUtils.isEmpty(keyWord)){
            return findAll();
        }
        return findAllByKeyWord(keyWord);
    }*/

    @Override
    public Map<String, List<String>> findAll() {
        return listFiles(Path.of(CACHE_DIR_PATH));
    }

    @Override
    public Map<String, List<String>> findAllByKeyWord(String keyWord) {
        return listFiles(Path.of(CACHE_DIR_PATH+ "/" + keyWord));
    }



    /*@Override
    public List<String> findAllByKeyWord(String keyWord) {
        File childDir = getChildDir(keyWord);
        if(!Files.isDirectory(childDir.toPath())){
            return Collections.emptyList();
        }
        return FileUtils.listFiles(childDir, null, true).stream()
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }*/

    /*@Override
    public List<String> findAll() {
        return FileUtils.listFiles(CACHE_DIR, null, true).stream()
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }*/

    @Override
    public String add(String keyWord, File gif) {
        File childDir = getChildDir(keyWord);
        try {
            FileUtils.copyFileToDirectory(gif, childDir);
        } catch (IOException e) {
            return "";
        }
        return childDir + gif.getName();

    }

    @Override
    public boolean clean() {
        try {
            FileUtils.cleanDirectory(CACHE_DIR);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public String findGifByKeyWord(String keyWord) {
        File childDir = getChildDir(keyWord);
        if(!Files.isDirectory(childDir.toPath())){
            return FILE_NOT_FOUND;
        }
        return FileUtils.listFiles(childDir, null, true).stream()
                .map(File::getAbsolutePath)
                .findAny()
                .orElse(FILE_NOT_FOUND);
    }

    private File getChildDir(String keyWord) {
        return new File(CACHE_DIR_PATH + "/" + keyWord);
    }

    @SneakyThrows
    private Map<String, List<String>> listFiles(final Path baseDir) {
        final Map<String, List<String>> map = new HashMap<>();
        try (final DirectoryStream<Path> stream = Files.newDirectoryStream(baseDir)) {
            for (final Path subdir : stream)
                populateMap(map, subdir);
        }

        return map;
    }

    @SneakyThrows
    private void populateMap(final Map<String, List<String>> map, final Path subdir) {
        final String dirname = subdir.getFileName().toString();
        if (Files.isDirectory(subdir)) {
            map.put(dirname, new ArrayList<>());
            try (final DirectoryStream<Path> stream = Files.newDirectoryStream(subdir)) {
                for (final Path entry : stream) {
                    if (Files.isDirectory(entry)) {
                        populateMap(map, entry);
                    }
                    if (!Files.isDirectory(entry)) {
                        map.get(dirname).add(entry.getFileName().toString());
                    }
                }
            }
        }
    }
}
