package com.bsa.bsagiphy.repository;

import java.io.File;

public interface CacheInMemoryRepository {
    boolean cleanCacheByKeyWord(String userName, String keyWord);
    boolean deleteCache(String userName);
    void save(String userName, String keyWord, File file);
    String find(String userName, String keyWord);
//    String addUser(String userName);
}
