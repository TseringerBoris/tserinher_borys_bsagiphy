package com.bsa.bsagiphy.repository;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CacheRepository {

    String add(String keyWord, File gif);

    Map<String, List<String>> findAllByKeyWord(String keyWord);

    boolean clean() throws IOException;

    String findGifByKeyWord(String keyWord);

    Map<String, List<String>> findAll();


}
