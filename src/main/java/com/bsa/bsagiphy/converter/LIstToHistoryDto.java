package com.bsa.bsagiphy.converter;

import com.bsa.bsagiphy.dto.HistoryDto;
import com.bsa.bsagiphy.model.HistoryEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LIstToHistoryDto implements Converter<List<HistoryEntity>, List<HistoryDto>> {
    @Override
    public List<HistoryDto> convert(List<HistoryEntity> source) {
        return source.stream()
                .map(historyEntity -> {
                    HistoryDto historyDto = new HistoryDto();
                    historyDto.setDate(historyEntity.getDate().format(DateTimeFormatter.ofPattern("dd-MM-uuuu")));
                    historyDto.setGif(historyEntity.getKeyWord());
                    historyDto.setKey(historyEntity.getPathToGif());
                    return historyDto;
                }).collect(Collectors.toList());

    }
}
