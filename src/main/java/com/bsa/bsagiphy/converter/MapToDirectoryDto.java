package com.bsa.bsagiphy.converter;

import com.bsa.bsagiphy.dto.*;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MapToDirectoryDto implements Converter<Map<String, List<String>>, List<DirDto>> {
    @Override
    public List<DirDto> convert(Map<String, List<String>> source) {
        return source.entrySet().stream()
                .map(stringListEntry -> {
                    DirDto dirDto = new DirDto();
                    dirDto.setQuery(stringListEntry.getKey());
                    dirDto.setGifs(stringListEntry.getValue());
                    return dirDto;
                }).collect(Collectors.toList());

    }
}
