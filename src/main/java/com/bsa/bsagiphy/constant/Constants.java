package com.bsa.bsagiphy.constant;


import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String FILE_NOT_FOUND = "not_found";
}
