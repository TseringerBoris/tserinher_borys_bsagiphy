package com.bsa.bsagiphy.exception.handler;

import com.bsa.bsagiphy.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleNotElementFoundException(NotFoundException exception) {
        final String supportMessage = "Not found TEST MY EXCEPTION ";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(supportMessage + exception.getLocalizedMessage());
    }
}
