package com.bsa.bsagiphy.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.Unirest;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class Utils {

    public static File download(String userName, String keyWord) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl = createGiphyEndpointRequest(keyWord);
        ResponseEntity<String> response
                = restTemplate.getForEntity(fooResourceUrl, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(Objects.requireNonNull(response.getBody()));
        String gifId = root.findValue("id").asText();
        String url = root.findValue("image_original_url").asText();
        String pathToDir = createPathToUserDir(userName, keyWord, gifId);
        if(userName.equals("cache")){
            if (Files.notExists(Path.of("./src/main/java/com/bsa/bsagiphy/bsa_giphy/cache/" + keyWord))) {
                Files.createDirectory(Path.of("./src/main/java/com/bsa/bsagiphy/bsa_giphy/cache/" + keyWord));
            }
        } else {
            if (Files.notExists(Path.of("./src/main/java/com/bsa/bsagiphy/bsa_giphy/users/" + userName))) {
                Files.createDirectory(Path.of("./src/main/java/com/bsa/bsagiphy/bsa_giphy/users/" + userName));
            }
            if (Files.notExists(Path.of("./src/main/java/com/bsa/bsagiphy/bsa_giphy/users/" + userName + "/" + keyWord))) {
                Files.createDirectory(Path.of("./src/main/java/com/bsa/bsagiphy/bsa_giphy/users/" + userName + "/" + keyWord));
            }
        }
        return Unirest.get(url).asFile(pathToDir).getBody();

    }

    private static String createGiphyEndpointRequest(String keyWord) {
        return String.format("http://api.giphy.com/v1/gifs/random?q=%s&api_key=9AEx4MmgowLriApMjLbBgZMHYxZomTgF&limit=1", keyWord);
    }

    private static String createPathToUserDir(String userName, String keyWord, String fileName) {
        return String.format("./src/main/java/com/bsa/bsagiphy/bsa_giphy/users/%s/%s/%s.gif", userName, keyWord, fileName);
    }
}

 /*void download(){
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = "http://api.giphy.com/v1/gifs/random?q=sun&api_key=9AEx4MmgowLriApMjLbBgZMHYxZomTgF&limit=1";
        ResponseEntity<String> response
                = restTemplate.getForEntity(fooResourceUrl, String.class);
        System.out.println(response.getStatusCode());

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        String id = root.findValue("id").asText();
        String url = root.findValue("image_original_url").asText();
        System.out.println("RESPONSE JSON: ID " + id + " URL " + url);

        File file = Unirest.get(url).asFile("./src/main/java/com/bsa/bsagiphy/bsa_giphy/cache/ball" + "/" + id + ".gif").getBody();

    }*/
