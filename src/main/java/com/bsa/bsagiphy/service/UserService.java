package com.bsa.bsagiphy.service;

import com.bsa.bsagiphy.model.HistoryEntity;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface UserService {

    Map<String, List<String>> getAllFilesBy(String userName);
    List<HistoryEntity> getUserHistory(String userName) throws IOException;
    boolean cleanUserHistory(String userName);
    String findGif(String userName, String keyWord, boolean isForce);
    String createGif(String userName, String keyWord, boolean isForce) throws IOException;
    boolean cleanUserCache(String userName, String keyWord);
    boolean deleteUserData(String userName);
    List<String> findAllGifs();
}
