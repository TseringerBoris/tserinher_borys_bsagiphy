package com.bsa.bsagiphy.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CacheService {

    String add(String keyWord, File gif);

    Map<String, List<String>> getCache(String keyWord);

    Map<String, List<String>> createGifAndReturnDir(String keyWord);

    boolean cleanCache();

    List<String> findAllGifs();
}
