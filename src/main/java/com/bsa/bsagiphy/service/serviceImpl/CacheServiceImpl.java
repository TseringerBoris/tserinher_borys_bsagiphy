package com.bsa.bsagiphy.service.serviceImpl;

import com.bsa.bsagiphy.repository.CacheInMemoryRepository;
import com.bsa.bsagiphy.repository.CacheRepository;
import com.bsa.bsagiphy.service.CacheService;
import com.bsa.bsagiphy.utils.Utils;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CacheServiceImpl implements CacheService {

    private CacheRepository cacheRepository;
    private CacheInMemoryRepository inMemoryRepository;

    public CacheServiceImpl(@Autowired CacheRepository cacheRepository, CacheInMemoryRepository inMemoryRepository) {
        this.cacheRepository = cacheRepository;
        this.inMemoryRepository = inMemoryRepository;
    }

    @Override
    public String add(String keyWord, File gif) {
        return cacheRepository.add(keyWord, gif);
    }

    @Override
    public Map<String, List<String>> getCache(String keyWord) {
        if(StringUtils.isEmpty(keyWord)){
            return cacheRepository.findAll();
        }
        return cacheRepository.findAllByKeyWord(keyWord);
    }

    @Override
    @SneakyThrows
    public Map<String, List<String>> createGifAndReturnDir(String keyWord){
        Utils.download("cache", keyWord);
        return cacheRepository.findAllByKeyWord(keyWord);

    }

    @Override
    @SneakyThrows
    public boolean cleanCache(){
        return cacheRepository.clean();
    }

    @SneakyThrows
    @Override
    public List<String> findAllGifs() {
        return cacheRepository.findAll().values().stream()
                .flatMap(Collection::stream).collect(Collectors.toList());
    }


}
