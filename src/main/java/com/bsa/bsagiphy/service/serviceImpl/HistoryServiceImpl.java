package com.bsa.bsagiphy.service.serviceImpl;

import com.bsa.bsagiphy.repository.HistoryRepository;
import com.bsa.bsagiphy.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {

    private HistoryRepository historyRepository;

    public HistoryServiceImpl(@Autowired HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    @Override
    public void addHistory() {

    }
}
