package com.bsa.bsagiphy.service.serviceImpl;

import com.bsa.bsagiphy.constant.Constants;
import com.bsa.bsagiphy.model.HistoryEntity;
import com.bsa.bsagiphy.repository.CacheInMemoryRepository;
import com.bsa.bsagiphy.repository.CacheRepository;
import com.bsa.bsagiphy.repository.FileSystemRepository;
import com.bsa.bsagiphy.repository.HistoryRepository;
import com.bsa.bsagiphy.service.CacheService;
import com.bsa.bsagiphy.service.UserService;
import com.bsa.bsagiphy.utils.Utils;
import kong.unirest.Unirest;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.bsa.bsagiphy.constant.Constants.*;

@Service
public class UserServiceImpl implements UserService {

    private FileSystemRepository fileSystemRepository;
    private CacheInMemoryRepository inMemoryRepository;
    private CacheRepository cacheRepository;
    private HistoryRepository historyRepository;
    private CacheService cacheService;

    public UserServiceImpl(FileSystemRepository fileSystemRepository,
                           CacheInMemoryRepository inMemoryRepository,
                           CacheRepository cacheRepository,
                           HistoryRepository historyRepository, CacheService cacheService) {
        this.fileSystemRepository = fileSystemRepository;
        this.inMemoryRepository = inMemoryRepository;
        this.cacheRepository = cacheRepository;
        this.historyRepository = historyRepository;
        this.cacheService = cacheService;
    }


    @Override
    public Map<String, List<String>> getAllFilesBy(String userName) {
        return fileSystemRepository.findAllByUserName(userName);
    }

    @Override
    public List<HistoryEntity> getUserHistory(String userName) throws IOException {
        return historyRepository.findByUser(userName);
    }

    @Override
    public boolean cleanUserHistory(String userName) {
        return historyRepository.delete(userName);
    }

    @Override
    public String findGif(String userName, String keyWord, boolean isForce) {
        String gifPath = isForce
                ? fileSystemRepository.find(userName, keyWord)
                : inMemoryRepository.find(userName, keyWord);
        inMemoryRepository.save(userName, keyWord, new File(gifPath));

        return gifPath;
    }

    @Override
    public String createGif(String userName, String keyWord, boolean isForce) throws IOException {

        if (isForce) {
            return downloadAndReturnPath(userName, keyWord);
        }

        String gifPath = cacheRepository.findGifByKeyWord(keyWord);

        if (gifPath.equals(FILE_NOT_FOUND)) {
            gifPath = downloadAndReturnPath(userName, keyWord);
            inMemoryRepository.save(userName, keyWord, new File(gifPath));
        }
        return gifPath;
    }

    private String downloadAndReturnPath(String userName, String keyWord) throws IOException {
        File gif = Utils.download(userName, keyWord);
        historyRepository.save(userName, new HistoryEntity(LocalDate.now(), keyWord, gif.getAbsolutePath()));
        return cacheService.add(keyWord, gif);
    }

    @Override
    public boolean cleanUserCache(String userName, String keyWord) {
        return inMemoryRepository.cleanCacheByKeyWord(userName, keyWord);
    }

    @Override
    public boolean deleteUserData(String userName) {
        return fileSystemRepository.deleteByKeyWord(userName, null);
    }

    @SneakyThrows
    @Override
    public List<String> findAllGifs() {
        return fileSystemRepository.findAll().values().stream()
                .flatMap(Collection::stream).collect(Collectors.toList());
    }

}
