package com.bsa.bsagiphy.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class UserEntity {
    private String userName;
    private Map<String, List<String>> userCache;
    private Map<String, Map<String, List<String>>> cache;
}
