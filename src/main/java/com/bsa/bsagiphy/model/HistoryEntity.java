package com.bsa.bsagiphy.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class HistoryEntity {

    private LocalDate date;
    private String keyWord;
    private String pathToGif;
}
